import { Streamlit } from "streamlit-component-lib"

// const span = document.body.appendChild(document.createElement("span"))
// const textNode = span.appendChild(document.createTextNode(""))
// const button = span.appendChild(document.createElement("button"))
// button.textContent = "Click Me!"

// let numClicks = 0
// let isFocused = false
// button.onclick = function () {
//   numClicks += 1
//   Streamlit.setComponentValue(numClicks)
// }

// button.onfocus = function () { isFocused = true }
// button.onblur = function () { isFocused = false }

let old_value;

function onRender(event) {
  const data = event.detail
  // ChemDoodle.ELEMENT['H'].jmolColor = 'black';
  // ChemDoodle.ELEMENT['S'].jmolColor = '#B9A130';
  // var sketcher = new ChemDoodle.SketcherCanvas('sketcher', 500, 300, { useServices: true, oneMolecule: true });
  // sketcher.styles.atoms_displayTerminalCarbonLabels_2D = true;
  // sketcher.styles.atoms_useJMOLColors = true;
  // sketcher.styles.bonds_clearOverlaps_2D = true;
  // sketcher.repaint();
  // if (data.theme) {
  //   const borderStyling = `1px solid var(${
  //     isFocused ? "--primary-color" : "gray"
  //   })`
  //   button.style.border = borderStyling
  //   button.style.outline = borderStyling
  // }
  // button.disabled = data.disabled
  // let name = data.args["name"]
  // textNode.textContent = `Hello, ${name}! ` + String.fromCharCode(160)

  old_value = sketcher.historyManager.undoStack.length

  setInterval(
    function () {
      if (old_value !== sketcher.historyManager.undoStack.length) {
        console.log('hello')
        old_value = sketcher.historyManager.undoStack.length

        let atoms = sketcher.getMolecule().atoms.map((b) => b.label)
        let bonds = sketcher.getMolecule().bonds.map((b) => [b.a1.label, b.a2.label])
        let rings = sketcher.getMolecule().rings.map((r) => {
          return {
            atoms: r.atoms.map((b) => b.label),
            bonds: r.bonds.map((b) => [b.a1.label, b.a2.label])
          }
        })

        Streamlit.setComponentValue(
          {
            history: sketcher.historyManager.undoStack.length,
            atoms: atoms,
            bonds: bonds,
            rings: rings
          }
        )
      }
    }, 
    500
    ); 

  Streamlit.setFrameHeight()
}

Streamlit.events.addEventListener(Streamlit.RENDER_EVENT, onRender)
Streamlit.setComponentReady()
Streamlit.setFrameHeight()