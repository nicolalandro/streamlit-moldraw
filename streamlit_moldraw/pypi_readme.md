# Streamlit Moldraw
A Streamlit library for create and open small molecule based on [chemdoddle web components](https://web.chemdoodle.com/)

|Darck theme|
|-----------|
|![streamlit app screenshot](https://gitlab.com/nicolalandro/streamlit-moldraw/-/raw/main/imgs/dark.png)|


To install:

```
pip install streamlit_moldraw
```

Example of usage:

```
from streamlit_moldraw import streamlit_moldraw

streamlit_moldraw()
```